import express from 'express';
import router from './routes/router';

const app: express.Application = express();

app.use('/api', router);

export default app;
