import { Request, Response } from 'express';
import EngineerModel from '../database/models/engineer';
import httpStatusCodes from '../utils/httpStatusCodes';
import spec from '../swagger.json';

// @ts-ignore
spec.paths['/engineers'] = {
    get: {
        tags: [
            'engineers',
        ],
        summary: 'Retrieve engineers',
        responses: {
            200: {
                description: 'Success',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {},
                            required: [],
                        },
                    },
                },
            },
        },
        security: [
            {
                bearer: [],
            },
        ],
        middleware: [],
    },
};

async function getEngineers(req: Request, res: Response) {
    try {
        const engineers = await EngineerModel.find();
        res.send(engineers);
    } catch (err) {
        res.status(httpStatusCodes.INTERNAL_SERVER_ERROR.code).send({ message: 'Something went wrong' });
    }
}

// @ts-ignore
spec.paths['/engineers'] = {
    // @ts-ignore
    ...spec.paths['/engineers'],
    post: {
        tags: [
            'engineers',
        ],
        summary: 'Create an engineer',
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            firstName: {
                                type: 'string',
                                description: 'engineer\'s first name',
                            },
                            lastName: {
                                type: 'string',
                                description: 'engineer\'s last name',
                            },
                        },
                        required: ['firstName', 'lastName'],
                    },
                },
            },
        },
        responses: {
            201: {
                description: 'Created',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {},
                            required: [],
                        },
                    },
                },
            },
        },
        security: [
            {
                bearer: [],
            },
        ],
        middleware: [],
    },
};

async function createEngineer(req: Request, res: Response) {
    const { firstName, lastName } = req.body;

    if (!firstName || !lastName) return res.status(httpStatusCodes.BAD_REQUEST.code).send({ message: 'Bad request' });

    try {
        const engineer = await EngineerModel.create({ firstName, lastName });
        res.setHeader('Location', engineer.id);

        return res.status(httpStatusCodes.CREATED.code).send();
    } catch (err) {
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR.code).send({ message: 'Something went wrong' });
    }
}

export default {
    getEngineers,
    createEngineer,
};
