import { Request, Response } from 'express';
import getFormattedDate, { getEndOfDayMs } from '../utils/date';
import httpStatusCodes from '../utils/httpStatusCodes';
import { filterEngineers } from '../rules/rules-service';
import EngineerModel from '../database/models/engineer';
import { IEngineer } from '../types';
import memCache from '../utils/memoryCache';
import spec from '../swagger.json';

// @ts-ignore
spec.paths['/spin-the-wheel'] = {
    get: {
        tags: [
            'wheel',
        ],
        summary: 'Spin the wheel!',
        responses: {
            200: {
                description: 'Success',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {},
                            required: [],
                        },
                    },
                },
            },
        },
        security: [
            {
                bearer: [],
            },
        ],
        middleware: [],
    },
};

async function spinTheWheel(req: Request, res: Response) {
    const date = getFormattedDate();
    const dayOfTheWeek = date.getDay();
    if (dayOfTheWeek === 6 || dayOfTheWeek === 0) {
        return res.status(httpStatusCodes.BAD_REQUEST.code).send({ message: 'It\'s weekend! We don\'t to that over here' });
    }
    try {
        const engineers = await filterEngineers();
        await EngineerModel.updateMany(
            {
                _id: { $in: engineers.map((engineer: IEngineer) => engineer._id) },
            },
            {
                $push: {
                    assignments: {
                        date,
                    },
                },
            },
        );
        memCache.put('wheel-result', engineers, getEndOfDayMs());
        return res.send(engineers);
    } catch (err) {
        console.log(err);
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR.code).send({ message: 'Something went wrong.' });
    }
}

export default {
    spinTheWheel,
};
