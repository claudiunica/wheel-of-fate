import { Request, Response } from 'express';
import httpStatusCodes from '../utils/httpStatusCodes';
import RuleModel from '../database/models/rule';
import spec from '../swagger.json';

// @ts-ignore
spec.paths['/rules'] = {
    post: {
        tags: [
            'rules',
        ],
        summary: 'Create a rule',
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            description: {
                                type: 'string',
                                description: 'description of the rule',
                            },
                            key: {
                                type: 'string',
                                description: 'key of the rule',
                            },
                            values: {
                                type: 'string',
                                description: 'object containing variables',
                            },
                            isActive: {
                                type: 'boolean',
                                description: 'used to activate/deactivate the rule',
                            },
                        },
                        required: ['description', 'key', 'values', 'isActive'],
                    },
                },
            },
        },
        responses: {
            201: {
                description: 'Created',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {},
                            required: [],
                        },
                    },
                },
            },
        },
        security: [
            {
                bearer: [],
            },
        ],
        middleware: [],
    },
};
async function createRule(req: Request, res: Response) {
    const {
        description, key, values, isActive,
    } = req.body;
    if (!description || !key || !values || !isActive) return res.status(httpStatusCodes.BAD_REQUEST.code).send({ message: 'Wrong input!' });
    try {
        const rule = await RuleModel.create({
            description, key, values, isActive,
        });

        res.setHeader('Location', rule.id);
        return res.send(rule);
    } catch (err) {
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR.code).send({ message: 'Something went wrong' });
    }
}

// @ts-ignore
spec.paths['/rules'] = {
    // @ts-ignore
    ...spec.paths['/rules'],
    put: {
        tags: [
            'rules',
        ],
        summary: 'Update an existing rule',
        parameters: [
            {
                name: 'id',
                in: 'query',
                type: 'number',
                required: true,
            },
        ],
        requestBody: {
            required: true,
            content: {
                'application/json': {
                    schema: {
                        type: 'object',
                        properties: {
                            description: {
                                type: 'string',
                                description: 'description of the rule',
                            },
                            key: {
                                type: 'string',
                                description: 'key of the rule',
                            },
                            values: {
                                type: 'string',
                                description: 'object containing variables',
                            },
                            isActive: {
                                type: 'boolean',
                                description: 'used to activate/deactivate the rule',
                            },
                        },
                        required: [],
                    },
                },
            },
        },
        responses: {
            201: {
                description: 'Created',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {},
                            required: [],
                        },
                    },
                },
            },
        },
        security: [
            {
                bearer: [],
            },
        ],
        middleware: [],
    },
};
async function updateRule(req: Request, res: Response) {
    const { id } = req.params;
    if (!id) return res.status(httpStatusCodes.BAD_REQUEST.code).send({ message: 'Missing id!' });
    try {
        await RuleModel.updateOne(
            { _id: id },
            req.body,
        );
        return res.send();
    } catch (err) {
        return res.status(httpStatusCodes.INTERNAL_SERVER_ERROR.code).send({ message: 'Something went wrong' });
    }
}

export default {
    createRule,
    updateRule,
};
