import assignmentRecurrency from './assignmentRecurrency';
import freeDays from './freeDays';
import workMultipleShifts from './workMultipleShifts';

const rulesDictionary: any = {
    assignmentRecurrency,
    freeDays,
    workMultipleShifts,
};

export default rulesDictionary;
