import EngineerModel from '../database/models/engineer';
import { IEngineer, IRule } from '../types';
import getFormattedDate from '../utils/date';

export default async (
    engineers: IEngineer[],
    rule: IRule,
    currentDate: Date,
): Promise<IEngineer[]> => {
    const { noRestDays } = rule.values;
    let date = getFormattedDate();
    date = new Date(date.setDate(currentDate.getDate() - noRestDays));

    const assignedEngineers = await EngineerModel.find({
        'assignments.date': { $gte: date },
    });

    const assignedEngineersIds = assignedEngineers.map((engineer: IEngineer) => engineer._id);

    return engineers.filter((engineer: IEngineer) => !assignedEngineersIds.includes(engineer._id));
};
