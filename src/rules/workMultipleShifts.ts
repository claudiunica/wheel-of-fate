import { IEngineer, IRule } from '../types';
import { NO_DAILY_ASSIGNMENTS } from '../utils/constants';

export default async (engineers: IEngineer[], rule: IRule): Promise<IEngineer[]> => {
    const { canWorkMultipleShifts } = rule.values;
    const filteredEngineers = engineers.slice(0, NO_DAILY_ASSIGNMENTS);

    if (!canWorkMultipleShifts) return filteredEngineers;

    const indexesToReturn: number[] = [];

    for (let i = 0; i < NO_DAILY_ASSIGNMENTS; i += 1) {
        const index = Math.floor(Math.random() * NO_DAILY_ASSIGNMENTS);
        indexesToReturn.push(index);
    }

    return indexesToReturn.map((index: number) => filteredEngineers[index]);
};
