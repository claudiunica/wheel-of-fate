import RuleModel from '../database/models/rule';
import EngineerModel from '../database/models/engineer';
import getFormattedDate from '../utils/date';
import { NO_DAILY_ASSIGNMENTS } from '../utils/constants';
import rulesDictionary from './index';
import { IRule } from '../types';

export const checkRules = (rules: IRule[], engineersLength: number) => {
    const recurrencyRule = rules.find((rule: IRule) => rule.key === 'assignmentRecurrency');
    const { noWeeks, noAssignments } = recurrencyRule && recurrencyRule.values;
    return 5 * noWeeks * NO_DAILY_ASSIGNMENTS === engineersLength * noAssignments;
};

export const filterEngineers = async () => {
    let engineers = await EngineerModel.find(
        {},
        {
            assignments: 0,
        },
    );
    const rules = await RuleModel.find().sort({ index: 1 });
    if (!checkRules(rules, engineers.length)) {
        throw new Error('Check rules.');
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const rule of rules) {
        // eslint-disable-next-line no-await-in-loop
        engineers = await rulesDictionary[rule.key](engineers, rule, getFormattedDate());
    }

    return engineers;
};
