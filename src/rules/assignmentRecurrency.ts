import getFormattedDate from '../utils/date';
import EngineerModel from '../database/models/engineer';
import { IEngineer, IRule } from '../types';

interface IEngineerAssignment extends IEngineer {
    noAssignments: number;
}

export default async (
    engineers: IEngineer[],
    rule: IRule,
    currentDate: Date,
): Promise<IEngineerAssignment[]> => {
    const { noAssignments, noWeeks } = rule.values;
    let date = getFormattedDate();
    date = new Date(date.setDate(currentDate.getDate() - noWeeks * 7));

    const pipeline = [
        {
            $project: {
                noAssignments: {
                    $size: {
                        $filter: {
                            input: '$assignments',
                            as: 'items',
                            cond: {
                                $gte: ['$$items.date', date],
                            },
                        },
                    },
                },
                firstName: '$firstName',
                lastName: '$lastName',
            },
        },
    ];
    const assignedEngineers: IEngineerAssignment[] = await EngineerModel.aggregate(pipeline);

    return assignedEngineers
        .filter((engineer: IEngineerAssignment) => engineer.noAssignments < noAssignments)
        .sort((a, b) => ((a.noAssignments > b.noAssignments) ? 1 : -1));
};
