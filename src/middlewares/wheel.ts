import { Request, Response, NextFunction } from 'express';
import memCache from '../utils/memoryCache';

export default async (req: Request, res: Response, next: NextFunction) => {
    const cached = memCache.get('wheel-result');
    if (cached) {
        return res.send(cached);
    }
    return next();
};
