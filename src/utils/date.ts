export default function getFormattedDate(date?: Date): Date {
    const formattedDate = date || new Date();
    return new Date(formattedDate.setHours(-(formattedDate.getTimezoneOffset() / 60), 0, 0, 0));
}

export function getEndOfDayMs() {
    const endDay = new Date().setHours(23, 59, 59, 999);
    return endDay - new Date().getTime();
}
