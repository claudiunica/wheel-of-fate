export default {
    OK: {
        code: 200,
        phrase: 'OK',
        description: 'indicates that the request has succeeded.',
    },
    CREATED: {
        code: 201,
        phrase: 'Created',
        description:
            'indicates that the request has been fulfilled and has resulted in one or more new resources being created.',
    },
    BAD_REQUEST: {
        code: 400,
        phrase: 'Bad Request',
        description:
            'indicates that the server cannot or will not process the request because the received syntax is invalid, nonsensical, or exceeds some limitation on what the server is willing to process.',
    },
    UNAUTHORIZED: {
        code: 401,
        phrase: 'Unauthorized',
        description:
            'indicates that the request has not been applied because it lacks valid authentication credentials for the target resource.',
    },
    FORBIDDEN: {
        code: 403,
        phrase: 'Forbidden',
        description: 'indicates that the server understood the request but refuses to authorize it.',
    },
    NOT_FOUND: {
        code: 404,
        phrase: 'Not Found',
        description:
            'indicates that the origin server did not find a current representation for the target resource or is not willing to disclose that one exists.',
    },
    INTERNAL_SERVER_ERROR: {
        code: 500,
        phrase: 'Internal Server Error',
        description:
            'indicates that the server encountered an unexpected condition that prevented it from fulfilling the request.',
    },
};
