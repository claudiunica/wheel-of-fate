import memoryCache, { CacheClass } from 'memory-cache';

const memCache: CacheClass<string, string> = new memoryCache.Cache();

export default memCache;
