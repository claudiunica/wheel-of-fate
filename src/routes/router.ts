import { Router, json } from 'express';
import swaggerUi from 'swagger-ui-express';
import engineerRouter from './engineerRouter';
import ruleRouter from './ruleRouter';
import wheelRouter from './wheelRouter';
import wheelMiddleware from '../middlewares/wheel';
import swaggerDocument from '../swagger.json';

const router = Router();

router.use(json());

router.use('/engineers', engineerRouter);
router.use('/rules', ruleRouter);
router.use('/spin-the-wheel', wheelMiddleware, wheelRouter);

router.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
export default router;
