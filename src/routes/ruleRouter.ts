import { Router } from 'express';
import ruleController from '../controllers/rule';

const ruleRouter = Router();

ruleRouter.post('/', ruleController.createRule);
ruleRouter.put('/:id', ruleController.updateRule);

export default ruleRouter;
