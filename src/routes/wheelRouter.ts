import { Router } from 'express';
import spinTheWheel from '../controllers/wheel';

const wheelRouter = Router();

wheelRouter.get('/', spinTheWheel.spinTheWheel);

export default wheelRouter;
