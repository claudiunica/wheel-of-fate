import { Router } from 'express';
import engineerController from '../controllers/engineer';

const engineerRouter = Router();

engineerRouter.get('/', engineerController.getEngineers);
engineerRouter.post('/', engineerController.createEngineer);

export default engineerRouter;
