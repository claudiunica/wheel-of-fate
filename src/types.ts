import { ObjectId } from 'mongoose';

export interface IEmployeeAssignment {
    date: Date;
}

export interface IEngineer {
    _id: ObjectId;
    firstName: string;
    lastName: string;
    assignments: IEmployeeAssignment;
}

export interface IRule {
    _id: ObjectId;
    description: string;
    key: string;
    values: any;
    isActive: true;
    index: number;
}
