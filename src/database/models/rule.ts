import mongoose, { Schema } from 'mongoose';

export const Rule = new Schema({
    description: String,
    key: String,
    values: Object,
    isActive: Boolean,
});

export default mongoose.model('rule', Rule);
