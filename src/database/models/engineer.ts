import mongoose, { Schema } from 'mongoose';

const assignmentSchema = new Schema({
    date: Date,
});

export const Engineer = new Schema({
    firstName: String,
    lastName: String,
    assignments: {
        type: [assignmentSchema],
        default: [],
    },
});

export default mongoose.model('engineer', Engineer);
