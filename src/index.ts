import { config } from 'dotenv';
import app from './app';
import connect from './database/db';
import router from './routes/router';

const PORT = process.env.PORT || 5000;

config();

connect().then(() => {
    app.use('/api', router);

    app.listen(process.env.PORT, () => {
        console.log(`Server started on port ${PORT}`);
    });
});
